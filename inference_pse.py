from torchvision import transforms
import cv2
import copy
import numpy as np
import torch
import hydra
from PIL import Image
from pytorch_lightning import LightningModule, Trainer, LightningDataModule
from omegaconf import DictConfig
from src.models.psenet_module import PSENetLitModule
import torchvision.models as models
# 自动下载网上的预训练模型
# resnet18_model = models.resnet18(pretrained=True)
# 手动导入预训练模型

@hydra.main(config_path="configs/", config_name="train.yaml")
def main(config: DictConfig):
    # ckpt can be also a URL!
    CKPT_PATH = 'G:/TuProjects/lightning-hydra-psenet/logs/experiments/runs/default/2022-06-29_19-42-05/checkpoints/epoch_047.ckpt'
    d = torch.load(CKPT_PATH)
    print(d['state_dict']['backbone.model.conv1.weight'])
    #print(d['backbone.model.conv1.weight'])
    #model: LightningModule = hydra.utils.instantiate(config.model)
    model: LightningModule = PSENetLitModule.load_from_checkpoint(CKPT_PATH)
    datamodule: LightningDataModule = hydra.utils.instantiate(config.datamodule)
    # trainer: Trainer = hydra.utils.instantiate(
    #     config.trainer, callbacks=None, logger=None, _convert_="partial"
    # )
    #
    # trainer.predict(model, datamodule=datamodule, dataloaders =datamodule, return_predictions=True, ckpt_path=CKPT_PATH)
    model.load_from_checkpoint(CKPT_PATH)
    print(model.backbone.model.conv1.weight)
    # switch to evaluation mode
    model.eval()
    model.freeze()
    #input = torch.randn(1,3,1024, 1024)
    #model.to_onnx('abc.onnx', input,opset_version=11)

    # load data
    #img = Image.open("data/example_img.png").convert("L")  # convert to black and white
    #img = Image.open("data/example_img.png").convert("RGB")  # convert to RGB
    img = cv2.imread('/TuProjects/lightning-hydra-psenet/img_2.jpg')
    img2 = copy.deepcopy(img)
    h, w, c = img2.shape
    long_size = 1024
    h, w = img.shape[:2]
    # if max(h, w) > long_size:
    scale = long_size / max(h, w)
    img = cv2.resize(img, None, fx=scale, fy=scale)
    img = Image.fromarray(img2)
    img = img.convert("RGB")
    img = transforms.ToTensor()(img)
    img = transforms.Normalize(
        mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]
    )(img)
    # 将图片由(w,h)变为(1,img_channel,h,w)
    #tensor = transforms.ToTensor()(img2)
    tensor = img.unsqueeze_(0)
    img_metas = {}
    img_metas['org_img_size'] = [[h, w]]
    img_metas['img_size'] = [[h, w]]
    outputs = model(tensor, img_metas=img_metas)
    print(outputs)
    # preds, boxes_list = pse_decode(preds[0], 1)
    # scale = (preds.shape[1] * 1.0 / w, preds.shape[0] * 1.0 / h)
    # if len(boxes_list):
    #     boxes_list = boxes_list / scale
    boxes_list = outputs['bboxes']
    #boxes_list = boxes_list.reshape(-1, 4, 1, 2).astype(np.int32).tolist()
    print(boxes_list)
    for box in boxes_list:
        cv2.polylines(img2, [np.array(box).reshape(4, 1, 2)], True, (0, 255, 0), 2)
    cv2.imwrite('a.jpg', img2)

if __name__ == "__main__":
    main()
