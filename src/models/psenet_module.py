from typing import Any, List

import torch
import torch.nn as nn
from pytorch_lightning import LightningModule
import torch.nn.functional as F
from src.models.utils.average_meter import AverageMeter
from src.evalutions.det_eval import DetMetric
from src.models.psenet.pse import decode as pse_decode
import cv2
from torchvision.transforms import transforms
from src.models.hmean_iou import eval_hmean_iou
import numpy as np


class PSENetLitModule(LightningModule):
    """
    Example of LightningModule for MNIST classification.

    A LightningModule organizes your PyTorch code into 5 sections:
        - Computations (init).
        - Train loop (training_step)
        - Validation loop (validation_step)
        - Test loop (test_step)
        - Optimizers (configure_optimizers)

    Read the docs:
        https://pytorch-lightning.readthedocs.io/en/latest/common/lightning_module.html
    """

    def __init__(
        self,
        backbone: torch.nn.Module,
        neck: torch.nn.Module,
        detection_head: torch.nn.Module,
        ratio=0.3,
        **kwargs
    ):
        super().__init__()

        # this line ensures params passed to LightningModule will be saved to ckpt
        # it also allows to access params with 'self.hparams' attribute
        self.save_hyperparameters()

        self.backbone = backbone
        self.fpn = neck
        self.det_head = detection_head

        self.losses = AverageMeter(max_len=500)
        self.losses_text = AverageMeter(max_len=500)
        self.losses_kernels = AverageMeter(max_len=500)
        self.losses_emb = AverageMeter(max_len=500)
        self.losses_rec = AverageMeter(max_len=500)

        self.ious_text = AverageMeter(max_len=500)
        self.ious_kernel = AverageMeter(max_len=500)
        self.accs_rec = AverageMeter(max_len=500)
        self.det_metric = DetMetric()

    def _upsample(self, x, size, scale=1):
        _, _, H, W = size
        return F.interpolate(x, size=(H // scale, W // scale), mode="nearest")
        # return F.upsample(x, size=(H // scale, W // scale), mode="bilinear")

    def forward(
        self,
        imgs: torch.Tensor,
        gt_texts=None,
        gt_kernels=None,
        training_masks=None,
        img_metas=None,
    ):

        outputs = dict()
        # backbone
        f = self.backbone(imgs)

        # FPN
        f1, f2, f3, f4 = self.fpn(f[0], f[1], f[2], f[3])

        f = torch.cat((f1, f2, f3, f4), dim=1)

        det_out = self.det_head(f)

        if self.training:
            det_out = self._upsample(det_out, imgs.size())
            det_loss = self.det_head.loss(det_out, gt_texts, gt_kernels, training_masks)
            outputs.update(det_loss)
        else:
            det_out = self._upsample(det_out, imgs.size(), 1)
            det_res = self.det_head.get_results(det_out, img_metas)
            outputs.update(det_res)
        return outputs

    def step(self, batch: Any):
        x, y, mask = batch["image"], batch["score_maps"], batch["training_mask"]
        logits = self.forward(x)
        loss_c, loss_s, loss = self.criterion(logits, y, mask)
        return loss

    def training_step(self, data: Any, batch_idx: int):
        # forward
        outputs = self.forward(**data)

        # detection loss
        loss_text = torch.mean(outputs["loss_text"])
        self.losses_text.update(loss_text.item(), data["imgs"].size(0))

        loss_kernels = torch.mean(outputs["loss_kernels"])
        self.losses_kernels.update(loss_kernels.item(), data["imgs"].size(0))
        if "loss_emb" in outputs.keys():
            loss_emb = torch.mean(outputs["loss_emb"])
            self.losses_emb.update(loss_emb.item(), data["imgs"].size(0))
            loss = loss_text + loss_kernels + loss_emb
        else:
            loss = loss_text + loss_kernels
        outputs.update(loss=loss)
        iou_text = torch.mean(outputs["iou_text"])
        self.ious_text.update(iou_text.item(), data["imgs"].size(0))
        iou_kernel = torch.mean(outputs["iou_kernel"])
        self.ious_kernel.update(iou_kernel.item(), data["imgs"].size(0))

        # recognition loss
        # if with_rec:
        #     loss_rec = outputs['loss_rec']
        #     valid = loss_rec > -EPS
        #     if torch.sum(valid) > 0:
        #         loss_rec = torch.mean(loss_rec[valid])
        #         losses_rec.update(loss_rec.item(), data['imgs'].size(0))
        #         loss = loss + loss_rec
        #
        #         acc_rec = outputs['acc_rec']
        #         acc_rec = torch.mean(acc_rec[valid])
        #         accs_rec.update(acc_rec.item(), torch.sum(valid).item())

        self.losses.update(loss.item(), data["imgs"].size(0))

        self.log("train/loss", loss, on_step=False, on_epoch=True, prog_bar=False)
        self.log(
            "train/iou_text", iou_text, on_step=False, on_epoch=True, prog_bar=True
        )
        # log train metrics
        # acc = self.train_accuracy(preds, targets)
        # self.log("train/loss", loss, on_step=False, on_epoch=True, prog_bar=False)
        # self.log("train/acc", acc, on_step=False, on_epoch=True, prog_bar=True)

        # we can return here dict with any tensors
        # and then read it in some callback or in training_epoch_end() below
        # remember to always return loss from training_step, or else backpropagation will fail!
        # print(outputs)
        return outputs

    def training_epoch_end(self, outputs: List[Any]):
        # `outputs` is a list of dicts returned from `training_step()`
        self.log("train/losses", self.losses.avg, on_step=False, on_epoch=True, prog_bar=False)
        # self.log(
        #     "train/iou_text", iou_text, on_step=False, on_epoch=True, prog_bar=True
        # )

        return


    def validation_step(self, batch: Any, batch_idx: int):
        # print(batch)
        outputs = self.forward(imgs=batch["imgs"], img_metas=batch["img_metas"])
        # print("outputs ", outputs)
        org_img_size = batch["img_metas"]["org_img_size"][0]
        img_height, img_width = int(org_img_size[0]), int(org_img_size[1])
        N, K, _ = batch["gt_bboxes"].shape
        gt_boxes = batch["gt_bboxes"].data.cpu().numpy().reshape(N, K, 4, 2)
        for k in range(K):
            bbox = gt_boxes[0, k]
            bbox[:, 0] *= img_width
            bbox[:, 1] *= img_height
            gt_boxes[0, k, :, :] = bbox

        gt_ignore_labels = np.zeros((N, K), dtype=np.int32)
        for k in range(K):
            if batch["gt_words"][k][0] == "###":
                gt_ignore_labels[0, k] = 1

        P_K = len(outputs["bboxes"])
        pred_bboxes = np.zeros((N, P_K, 4, 2))
        for k in range(P_K):
            bbox = outputs["bboxes"][k].reshape(4, 2)
            pred_bboxes[0, k, :, :] = bbox

        gt_batch = [None, None, gt_boxes, gt_ignore_labels]
        self.det_metric(preds=pred_bboxes, batch=gt_batch)

        return outputs

        batch_size = len(batch["file_name"])

        pred_boxes_list = []
        gt_boxes_list = []
        gt_ignored_boxes = []
        for i in range(batch_size):
            file_name = batch["file_name"][i]
            gt_boxes = batch["gt_boxes"][i]
            gt_boxes_count = batch["gt_boxes_count"][i]
            gt_boxes = gt_boxes[:gt_boxes_count, :, :]
            gt_boxes = gt_boxes.reshape(-1, 8).tolist()

            img = cv2.imread(file_name)
            long_size = 1024
            h, w = img.shape[:2]
            # if max(h, w) > long_size:
            scale = long_size / max(h, w)
            img = cv2.resize(img, None, fx=scale, fy=scale)
            # 将图片由(w,h)变为(1,img_channel,h,w)
            tensor = transforms.ToTensor()(img)
            tensor = tensor.unsqueeze_(0).to("cuda")
            preds = self.forward(tensor)
            preds, boxes_list = pse_decode(preds[0], 1)
            scale = (preds.shape[1] * 1.0 / w, preds.shape[0] * 1.0 / h)
            if len(boxes_list):
                boxes_list = boxes_list / scale
            boxes_list = boxes_list.reshape(-1, 8).tolist()
            pred_boxes_list.append(boxes_list)
            gt_boxes_list.append(gt_boxes)
            gt_ignored_boxes.append([[0, 0, 0, 0, 0, 0, 0, 0]])
        eval_result = eval_hmean_iou(pred_boxes_list, gt_boxes_list, gt_ignored_boxes)

        self.log(
            "val/recall",
            float(eval_result[0]["recall"]),
            on_step=False,
            on_epoch=True,
            prog_bar=False,
        )
        self.log(
            "val/precision",
            float(eval_result[0]["precision"]),
            on_step=False,
            on_epoch=True,
            prog_bar=False,
        )
        self.log(
            "val/hmean",
            float(eval_result[0]["hmean"]),
            on_step=False,
            on_epoch=True,
            prog_bar=False,
        )
        return eval_result

    def validation_epoch_end(self, outputs: List[Any]):
        metric = self.det_metric.get_metric()
        self.log(
            "validation/precision",
            metric["precision"],
            on_step=False,
            on_epoch=True,
            prog_bar=False,
        )

        self.log(
            "validation/recall",
            metric["recall"],
            on_step=False,
            on_epoch=True,
            prog_bar=False,
        )
        self.log(
            "validation/hmean",
            metric["hmean"],
            on_step=False,
            on_epoch=True,
            prog_bar=False,
        )
        return

    def test_step(self, batch: Any, batch_idx: int):
        outputs = self.forward(imgs=batch["imgs"], img_metas=batch["img_metas"])
        # print("outputs ", outputs)
        org_img_size = batch["img_metas"]["org_img_size"][0]
        img_height, img_width = int(org_img_size[0]), int(org_img_size[1])
        N, K, _ = batch["gt_bboxes"].shape
        gt_boxes = batch["gt_bboxes"].data.cpu().numpy().reshape(N, K, 4, 2)
        for k in range(K):
            bbox = gt_boxes[0, k]
            bbox[:, 0] *= img_width
            bbox[:, 1] *= img_height
            gt_boxes[0, k, :, :] = bbox

        gt_ignore_labels = np.zeros((N, K), dtype=np.int32)
        for k in range(K):
            if batch["gt_words"][k][0] == "###":
                gt_ignore_labels[0, k] = 1

        P_K = len(outputs["bboxes"])
        pred_bboxes = np.zeros((N, P_K, 4, 2))
        for k in range(P_K):
            bbox = outputs["bboxes"][k].reshape(4, 2)
            pred_bboxes[0, k, :, :] = bbox

        gt_batch = [None, None, gt_boxes, gt_ignore_labels]
        self.det_metric(preds=pred_bboxes, batch=gt_batch)

        return outputs
        
        outputs = self.forward(**batch)

        batch_size = len(batch["file_name"])

        pred_boxes_list = []
        gt_boxes_list = []
        gt_ignored_boxes = []
        for i in range(batch_size):
            file_name = batch["file_name"][i]
            gt_boxes = batch["gt_boxes"][i]
            gt_boxes_count = batch["gt_boxes_count"][i]
            gt_boxes = gt_boxes[:gt_boxes_count, :, :]
            gt_boxes = gt_boxes.reshape(-1, 8).tolist()

            img = cv2.imread(file_name)
            long_size = 1024
            h, w = img.shape[:2]
            # if max(h, w) > long_size:
            scale = long_size / max(h, w)
            img = cv2.resize(img, None, fx=scale, fy=scale)
            # 将图片由(w,h)变为(1,img_channel,h,w)
            tensor = transforms.ToTensor()(img)
            tensor = tensor.unsqueeze_(0).to("cuda")
            preds = self.forward(tensor)
            preds, boxes_list = pse_decode(preds[0], 1)
            scale = (preds.shape[1] * 1.0 / w, preds.shape[0] * 1.0 / h)
            if len(boxes_list):
                boxes_list = boxes_list / scale
            boxes_list = boxes_list.reshape(-1, 8).tolist()
            pred_boxes_list.append(boxes_list)
            gt_boxes_list.append(gt_boxes)
            gt_ignored_boxes.append([[0, 0, 0, 0, 0, 0, 0, 0]])
        eval_result = eval_hmean_iou(pred_boxes_list, gt_boxes_list, gt_ignored_boxes)

        self.log(
            "test/recall",
            float(eval_result[0]["recall"]),
            on_step=False,
            on_epoch=True,
            prog_bar=False,
        )
        self.log(
            "test/precision",
            float(eval_result[0]["precision"]),
            on_step=False,
            on_epoch=True,
            prog_bar=False,
        )
        self.log(
            "test/hmean",
            float(eval_result[0]["hmean"]),
            on_step=False,
            on_epoch=True,
            prog_bar=False,
        )
        return eval_result

    def test_epoch_end(self, outputs: List[Any]):
        pass

    def configure_optimizers(self):
        """Choose what optimizers and learning-rate schedulers to use in your optimization.
        Normally you'd need one. But in the case of GANs or similar you might have multiple.

        See examples here:
            https://pytorch-lightning.readthedocs.io/en/latest/common/lightning_module.html#configure-optimizers
        """
        return torch.optim.Adam(
            params=self.parameters(),
            lr=self.hparams.lr,
            weight_decay=self.hparams.weight_decay,
        )


if __name__ == "__main__":
    print("hello")
    model = PSENetLitModule()
    input = torch.randn((1, 3, 640, 640))
    model.to_onnx("a.onnx", input, export_params=True, opset_version=11)
