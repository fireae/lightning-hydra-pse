import os
import pathlib
from torch.utils import data
import glob
import numpy as np
import cv2
from src.datamodules.psenet.utils import draw_bbox
from src.datamodules.psenet.utils import make_pse_label


class ICDAR2015_Dataset(data.Dataset):
    def __init__(self, img_dir, gt_dir, data_shape: int = 640, n=6, m=0.5, transform=None, target_transform=None):
        self.data_list = self.load_data(gt_dir, img_dir)
        self.data_shape = data_shape
        self.transform = transform
        self.target_transform = target_transform
        self.n = n
        self.m = m

    def __getitem__(self, index):
        # print(self.image_list[index])
        img_path, text_polys, text_tags = self.data_list[index]
        img, score_maps, training_mask = make_pse_label(img_path, np.array(text_polys, dtype=np.float32),
                                                        np.array(text_tags, dtype=np.bool), input_size=self.data_shape,
                                                        n=self.n,
                                                        m=self.m)
        if self.transform:
            img = self.transform(img)
        if self.target_transform:
            score_maps = self.target_transform(score_maps)
            training_mask = self.target_transform(training_mask)
        # print(img.shape, type(img), score_maps.shape, type(score_maps),  training_mask.shape, type(training_mask))

        # return img, score_maps, training_mask
        # print(len(text_polys), text_polys)
        text_polys_count = len(text_polys)
        np_text_polys = np.array(text_polys, dtype=np.float32)
        np_1 = np.zeros((100, 4, 2), dtype=np.float32)
        np_1[:text_polys_count, :, :] = np_text_polys
        sample = {"file_name": img_path, "gt_boxes": np_1, "gt_boxes_count": text_polys_count, "image": img,
                  "score_maps": score_maps, "training_mask": training_mask}
        return sample

    def load_data(self, gt_dir, img_dir: str) -> list:
        data_list = []
        for x in glob.glob(img_dir + '/*.jpg', recursive=True):
            d = pathlib.Path(x)
            label_path = os.path.join(gt_dir, (str(d.stem) + '.txt'))
            bboxs, text_tags = self._get_annotation(label_path)
            if len(bboxs) > 0:
                data_list.append((x, bboxs, text_tags))
            else:
                print('there is no suit bbox on {}'.format(label_path))
        return data_list

    def _get_annotation(self, label_path: str) -> tuple:
        boxes = []
        text_tags = []
        with open(label_path, encoding='utf-8', mode='r') as f:
            for line in f.readlines():
                params = line.strip().strip('\ufeff').strip('\xef\xbb\xbf').split(',')
                params.append('text')
                try:
                    label = params[8]
                    if label == '*' or label == '###':
                        text_tags.append(True)
                    else:
                        text_tags.append(False)
                    # if label == '*' or label == '###':
                    x1, y1, x2, y2, x3, y3, x4, y4 = list(map(float, params[:8]))
                    boxes.append([[x1, y1], [x2, y2], [x3, y3], [x4, y4]])
                except:
                    print('load label failed on {}'.format(label_path))
        # return np.array(boxes, dtype=np.float32), np.array(text_tags, dtype=np.bool)
        return boxes, text_tags

    def __len__(self):
        return len(self.data_list)

    def save_label(self, img_path, label):
        save_path = img_path.replace('img', 'save')
        if not os.path.exists(os.path.split(save_path)[0]):
            os.makedirs(os.path.split(save_path)[0])
        img = draw_bbox(img_path, label)
        cv2.imwrite(save_path, img)
        return img


if __name__ == '__main__':
    import torch
    import config
    from utils.utils import show_img
    from tqdm import tqdm
    from torch.utils.data import DataLoader
    import matplotlib.pyplot as plt
    from torchvision import transforms

    train_data = ICDAR2015_Dataset(config.trainroot, data_shape=config.data_shape, n=config.n, m=config.m,
                                   transform=transforms.ToTensor())
    train_loader = DataLoader(dataset=train_data, batch_size=1, shuffle=False, num_workers=0)

    pbar = tqdm(total=len(train_loader))
    for i, (img, label, mask) in enumerate(train_loader):
        print(label.shape)
        print(img.shape)
        print(label[0][-1].sum())
        print(mask[0].shape)
        # pbar.update(1)
        show_img((img[0] * mask[0].to(torch.float)).numpy().transpose(1, 2, 0), color=True)
        show_img(label[0])
        show_img(mask[0])
        plt.show()

    pbar.close()
